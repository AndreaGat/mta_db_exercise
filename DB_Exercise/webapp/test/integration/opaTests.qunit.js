/* global QUnit */
QUnit.config.autostart = false;

sap.ui.getCore().attachInit(function () {
	"use strict";

	sap.ui.require([
		"DB_Exercise/DB_Exercise/test/integration/AllJourneys"
	], function () {
		QUnit.start();
	});
});