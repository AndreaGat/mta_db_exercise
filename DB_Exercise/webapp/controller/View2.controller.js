sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel"
], function (Controller, JSONModel) {
	"use strict";

	return Controller.extend("DB_Exercise.DB_Exercise.controller.View2", {

		/**
		 * Called when a controller is instantiated and its View controls (if available) are already created.
		 * Can be used to modify the View before it is displayed, to bind event handlers and do other one-time initialization.
		 * @memberOf DB_Exercise.DB_Exercise.view.View2
		 */
		onInit: function () {
			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.getRoute("View2").attachMatched(this._onRouteMatched, this);
		},
		_onRouteMatched: function (oEvent) {
			var id = oEvent.getParameter("arguments").id;
			this.odataFunctionRead(id);
		},
		odataFunctionRead: function (id) {
			sap.ui.core.BusyIndicator.show(0);
			var that = this;
			var serviceUrl = "/DB_ExerciseDB_Exercise/dbExercise/odata/v2/CatalogService/";
			var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);
			modelColl.read("/Product(" + id + ")", {
				async: true,
				success: function (oResult) {
					sap.ui.core.BusyIndicator.hide();
					var jsonModel = new JSONModel(oResult);
					that.getView().setModel(jsonModel, "listModelProduct");

				},
				error: function (oError) {
					var dataModel = that.getView().getModel("listModelProduct");
					if (dataModel) {
						dataModel.setData(null);
					}
					sap.ui.core.BusyIndicator.hide();
					var msg = 'Attenzione errore';
					sap.m.MessageToast.show(msg);
				}
			});
			// ...
			// ...
		},
		navView1: function () {
			this.getOwnerComponent().getRouter().navTo("TargetView1");
		}

		/**
		 * Similar to onAfterRendering, but this hook is invoked before the controller's View is re-rendered
		 * (NOT before the first rendering! onInit() is used for that one!).
		 * @memberOf DB_Exercise.DB_Exercise.view.View2
		 */
		//	onBeforeRendering: function() {
		//
		//	},

		/**
		 * Called when the View has been rendered (so its HTML is part of the document). Post-rendering manipulations of the HTML could be done here.
		 * This hook is the same one that SAPUI5 controls get after being rendered.
		 * @memberOf DB_Exercise.DB_Exercise.view.View2
		 */
		//	onAfterRendering: function() {
		//
		//	},

		/**
		 * Called when the Controller is destroyed. Use this one to free resources and finalize activities.
		 * @memberOf DB_Exercise.DB_Exercise.view.View2
		 */
		//	onExit: function() {
		//
		//	}

	});

});