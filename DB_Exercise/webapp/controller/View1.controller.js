sap.ui.define([
	"sap/ui/core/mvc/Controller",
	"sap/ui/model/json/JSONModel",
	"sap/ui/model/Filter"
], function (Controller, JSONModel, Filter) {
	"use strict";

	return Controller.extend("DB_Exercise.DB_Exercise.controller.View1", {
		onInit: function () {
			this.getView().setBusy(true);

			var that = this;
			var serviceUrl = "/DB_ExerciseDB_Exercise/dbExercise/odata/v2/CatalogService/";
			var modelColl = new sap.ui.model.odata.v2.ODataModel(serviceUrl);

			//Read Product entity
			modelColl.read("/Product", {
				async: true,
				urlParameters: {
					"$expand": "category"
				},
				success: function (oResult) {
					that.getView().setBusy(false);
					var results = oResult.results;

					console.log(results);

					var oModel = new JSONModel({
						Product: results
					});
					that.getView().setModel(oModel, "listModel");
				},
				error: function (oError) {
					that.getView().setBusy(false);
				}
			});
		},
		onItemPress: function (oEvent) {
			var oItem = oEvent.getSource();
			var oCtx = oItem.getBindingContext("listModel");
			var id = oCtx.getProperty("ID");

			var oRouter = sap.ui.core.UIComponent.getRouterFor(this);
			oRouter.navTo("View2", {
				id: id
			});
		},
		buttonColor: function (parameter) {
			if (parameter < 50) {
				return "Success";
			} else if (parameter > 1000) {
				return "Error";
			} else {
				return "None";
			}
		},
		filterGlobally: function (oEvent) {
			// var sQuery = oEvent.getParameter("query");
			// this._oGlobalFilter = null;

			// var valueFilter = sQuery,
			// 	oBinding = this.byId("idProductsTable").getBinding("items"),
			// 	oFilter;

			// if (valueFilter) {
			// 	//this._oTable.setShowOverlay(false);
			// 	oFilter = new Filter("prodName", "EQ", valueFilter);
			// 	oBinding.filter([oFilter]);
			// }
		}
	});
});